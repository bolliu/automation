from utils.simple import set_func_attr
import SQLInterface
import Environment

class Attribute:
    def __init__(self, datestamp, value):
        self.nick = ''  # will be overwritten
        self.datestamp = datestamp
        self.spec = ''
        self.value = value
        self.nominal = False
        self.tool = ''

    def serialize(self):
        SQLInterface.push_sparse(self)
        pass

    def parse_spec(self):
        if len(self.spec.split(',')) == 2:
            self.spec = [float(i) for i in self.spec.split(',')]
        else:
            self.spec = self.spec

    def get_nominal(self):
        self.parse_spec()
        if type(self.spec) == type([]):
            return self.spec[0] < self.value < self.spec[1]
        return self.spec == self.value


class SimpleAttribute:
    def __init__(self, name, search, index):
        self.name = name
        self.search = search
        self.index = index

    def convert_to_attribute(self, lineobj):
        value = lineobj.select(self.search, self.index)
        temp_attr = Attribute(lineobj.datestamp, value)
        temp_attr.tool = lineobj.file.tool
        temp_attr.spec = Environment.Tool.get_specs_from_name(toolname=temp_attr.tool)[self.name]
        temp_attr.nominal = temp_attr.get_nominal()
        return temp_attr


@set_func_attr(filetype='ChamberPump')
def analyzercollection_chamberpump(lineobj):
    simple_lib = {SimpleAttribute(),
                  SimpleAttribute(),
                  SimpleAttribute()}
    attributes = set()
    for simple_attribute in simple_lib:
        attributes.add(simple_attribute.convert_to_attribute(lineobj))
    return attributes




