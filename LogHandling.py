import datetime
import os
import codecs
from Environment import active_environment
from utils.simple import none_given, reverse_readline, initialize_register, getnumpiece
import FilenameFormat
import LoglineFormat


class LogFile:
    registry = initialize_register(FilenameFormat, 'name_in_file')

    def __init__(self, file_raw, path=active_environment.input_registry):
        self.filename_full = file_raw
        try:
            self.logtype, self._datestr = self.filename_full.split("_")
        except IndexError:
            print('invalid filename (no underscore found)')
        self.path = path
        self.iteration_of_this_day = 0
        self.date = datetime.date(1, 1, 1)
        self.lines = []
        self.tool = ''

    def parse_filename(self):
        parser = self.get_filename_parser()
        self.date = parser(self._datestr)
        self.iteration_of_this_day = self.check_for_multiple_files_under_this_day(self._datestr)

    def get_filename_parser(self):
        if self.logtype in self.registry:
            return self.registry[self.logtype]
        else:
            return self.registry['default']

    def parse_loglines(self, path=None):
        if none_given(path):
            path = self.path
        temp_path = os.path.join(path, self.filename_full)
        this_log = codecs.open(temp_path, 'r', encoding="utf-8-sig")
        self.lines = [LogLine(raw_input=line, file=self) for line in this_log]

    def check_for_multiple_files_under_this_day(self, datestring):
        if len(datestring) >= 11:
            return self.filename_full.split("_", 2)[1]

    @classmethod
    def full_load(cls, file_raw, path):
        obj = cls.__init__(file_raw, path)
        obj.parse_filename()
        obj.parse_loglines(path=path)

    def parse_loglines_reverse(self, path, timestamp):
        found_valid_datestamp = True
        reversed_lines = []
        while found_valid_datestamp:
            consider_line = reverse_readline(os.path.join(path, self.filename_full))
            try:
                new_log_line = LogLine(raw_input=consider_line, file=self)
                found_valid_datestamp = new_log_line.timestamp == timestamp          # todo: highly suspect
            except IndexError:
                found_valid_datestamp = False
            try:
                reversed_lines.append(LogLine(raw_input=consider_line, file=self))
            except IndexError:
                print('Partial line detected, skipping')
        reversed_lines.reverse()
        _ = reversed_lines.pop(0)
        if reversed_lines[-1].timestamp > datetime.datetime.now()+ datetime.timedelta(seconds=-30):
            _ = reversed_lines.pop(-1)
        return reversed_lines

# todo: make errors from logline mutable
# make sure that datestamp read is fully correct before proceeding.
class LogLine:
    registry = initialize_register(LoglineFormat, 'filetype')

    def __init__(self, raw_input, file):
        self.timestamp = datetime.datetime(1,1,1,1,1,1)
        self.text = ""
        self.raw = self.remove_nonnumerical_header_text(raw_input)
        self.file = file

    @staticmethod
    def remove_nonnumerical_header_text(raw):
        return raw[[x.isdigit() for x in raw].index(True):]

    def __str__(self):
        return self.raw

    def parse(self):
        parser = self.get_parser()
        self.timestamp = parser(self.timestamp)

    def get_parser(self):
        try:
            if self.logtype in self.registry:
                return self.registry[self.logtype]
            else:
                return self.registry['default']
        finally:
            pass

    def select(self, searchtext, nth):
        data = []
        if searchtext in self.text:
            nuarray = getnumpiece(self.text)
            try:
                if nth == []:
                    data.append([self.timestamp, []])
                elif nth == 'ALL':
                    data.append([self.timestamp, self.text])
                elif type(nth) == int:
                    data.append([self.timestamp, nuarray[nth]])
                else: #its an array!
                    data.append([self.timestamp, [nuarray[j] for j in nth]])
            except:
                print("Couldn't find the " + str(nth) + "th numerical in logline:" + self.text)
