from LogHandling import *
import unittest
import os
import codecs


class testEnvironment(unittest.TestCase):
    def test_testing_directory_exists(self):
        _test_env = Environment('test')
        self.assertTrue(len(os.listdir(_test_env.logfile_path)) > 0)

    def test_environment_registry_all_valid(self):
        i= 0
        envs = []
        for Env_register in Environment.filepath_registry:
            with self.subTest(i=i):
                tempenv = Environment(Env_register)
                self.assertNotEqual(tempenv, None)
                envs.append(tempenv)
            i += 1


class testFormats(unittest.TestCase):
    def test_created_registry(self):
        LogFile.register_filenameformat()
        self.assertTrue(len(LogFile.registry) > 0)


class testInit(unittest.TestCase):
    def setUp(self):
        self.testfile = LogFile('_')

    def test_default_iter_is_zero(self):
        self.assertFalse(self.testfile.iteration_of_this_day)

    def test_default_datetime_is_jan_first(self):
        self.assertEqual(self.testfile.date, datetime.date(1, 1, 1))


class testDefaultParserSelect(unittest.TestCase):
    def setUp(self):
        default_parseable = 'MachineControl_2019-08-02'
        self.testfile = LogFile(default_parseable)

    def test_no_multiple_days(self):
        self.assertFalse(self.testfile.check_for_multiple_files_under_this_day('2019-08-02'))

    def test_select_default_parser(self):
        self.assertEqual(self.testfile.get_filename_parser(), self.testfile._default_parse)

    def test_datestring_correct(self):
        self.assertEqual(self.testfile._datestr, '2019-08-02')

    def test_first_section_correct(self):
        self.assertEqual(self.testfile.logtype, 'MachineControl')


class testDefaultParser(unittest.TestCase):
    def setUp(self):
        default_parseable = 'MachineControl_2019-08-02'
        self.testfile = LogFile(default_parseable)
        self.testfile.parse_filename()

    def test_datestring_correct(self):
        self.assertEqual(self.testfile.date, datetime.date(2019, 8, 2))


class testGunParser(unittest.TestCase):
    def setUp(self):
        default_parseable = 'PgunItip_2019-mon-8'
        self.testfile = LogFile(default_parseable)
        self.testfile.parse_filename()

    def test_datestring_correct(self):
        self.assertEqual(self.testfile.date, datetime.date(2019, 8, 15))


class testEMGParser(unittest.TestCase):
    def setUp(self):
        default_parseable = 'EMGdata_20190802'
        self.testfile = LogFile(default_parseable)
        self.testfile.parse_filename()

    def test_datestring_correct(self):
        self.assertEqual(self.testfile.date, datetime.date(2019, 8, 2))


class testLogLineIn(unittest.TestCase):
    def setUp(self) -> None:
        _test_env = Environment('test')
        testLog = LogFile('ChamberPump_2019-08-02.log', _test_env)
        testLog.parse_filename()
        testLog.parse_loglines()




# todo
# test validity of file_raw
# test addition of new file type
# test full array of names
# refactor testremovescraptest



'''
class testRemoveScrapText(self):
    def setUp(self):
        testdoc =  codecs.open(os.path.join(os.getcwd(), os.listdir()[1]), 'r', encoding="utf-8-sig")

    def test_line_load(self):
        for line in testdoc
'''


# environment unittest
#

if __name__ == '__main__':
    unittest.main()


