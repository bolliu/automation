import os
import csv


def csv_to_nested_dict(path):
    with open(path, encoding='utf-8-sig') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        output = {}
        for line in csv_reader:
            temp_dict = dict(line)
            output[temp_dict.pop('key')] = temp_dict
        return output

mypath = os.path.join(os.getcwd(),'configs','spec.csv')
test = csv_to_nested_dict(mypath)

def flatten(inputlist):
    return [item for sublist in inputlist for item in sublist]


def none_given(myinput):
    return isinstance(myinput, type(None))


def filter_methods_tag(cls, tag=None):
    if none_given(tag):
        tag = cls.__name__.lower()

    def is_method_tagged(method_str):
        if len(method_str) > len(tag):
            return method_str[0:len(tag)] == tag
        else:
            return False

    method_strings = [func for func in dir(cls) if callable(getattr(cls, func))]
    filtered_method_strings = filter(is_method_tagged, method_strings)
    return [getattr(cls, i) for i in filtered_method_strings]


def initialize_register(cls, key_attr):
    register = {}
    if none_given(key_attr):
        key_attr = '__name__'
    for method in filter_methods_tag(cls):
        register[getattr(method, key_attr)] = method
    return register


def set_func_attr(**kwargs):
    def decorator_set_unit(func):
        for i in kwargs:
            setattr(func, i, kwargs[i])
        return func
    return decorator_set_unit


def reverse_readline(filename, buf_size=8192):
    """A generator that returns the lines of a file in reverse order"""
    with open(filename) as fh:
        segment = None
        offset = 0
        fh.seek(0, os.SEEK_END)
        file_size = remaining_size = fh.tell()
        while remaining_size > 0:
            offset = min(file_size, offset + buf_size)
            fh.seek(file_size - offset)
            buffer = fh.read(min(remaining_size, buf_size))
            remaining_size -= buf_size
            lines = buffer.split('\n')
            # The first line of the buffer is probably not a complete line so
            # we'll save it and append it to the last line of the next buffer
            # we read
            if segment is not None:
                # If the previous chunk starts right from the beginning of line
                # do not concat the segment to the last line of new chunk.
                # Instead, yield the segment first
                if buffer[-1] != '\n':
                    lines[-1] += segment
                else:
                    yield segment
            segment = lines[0]
            for index in range(len(lines) - 1, 0, -1):
                if lines[index]:
                    yield lines[index]
        # Don't yield None if the file was empty
        if segment is not None:
            yield segment


def nearest(items, pivot):
    """
    This function will return the datetime in items which is the closest to the date pivot.
    :param items: list of datetimes
    :param pivot: thing that you want to find the closest to
    :return: the thing in items that is closest to pivot
    """
    return min(items, key=lambda x: abs(x - pivot))


def getnumpiece(inputstr):
    '''
    :param inputstr: any string
    :return: return a list of strings, where each string is a float that was expressed in the original input string
    '''
    data = []
    dump = ""
    for i in inputstr:
        if i.isdigit() or i == "." or i=="-" or i =="+" or i=="E" or i=="e":
            dump = dump + i
        else:
            try:
                data.append(float(dump))
                dump = ""
            except ValueError:
                dump = ""
    if not dump == []:
        try:
            data.append(float(dump))
        except ValueError:
            dump = ""
    return data

