# Do not add any functions that start with this file's name

from utils.simple import set_func_attr
import datetime


@set_func_attr(filetype='imagelog')
def loglineformat_default(raw):
    mon = int(raw[5:7])
    day = int(raw[8:10])
    year = int(raw[0:4])
    hour = int(raw[11:13])
    mint = int(raw[14:16])
    sec = int(raw[17:19])
    ms = int(raw[20:23]) * 1000
    return datetime.datetime(year, mon, day, hour, mint, sec, ms)


@set_func_attr(filetype='ResultManager')
def loglineformat_default(raw):
    mon = int(raw[0:2])
    day = int(raw[3:5])
    year = int(raw[6:10])
    hour = int(raw[11:13])
    mint = int(raw[14:16])
    sec = int(raw[17:19])
    ms = int(raw[20:23]) * 1000
    return datetime.datetime(year, mon, day, hour, mint, sec, ms)


@set_func_attr(filetype='Apt')
def loglineformat_default(raw):
    mon = int(raw[0:2])
    day = int(raw[3:5])
    year = int(raw[6:10])
    hour = int(raw[11:13])
    mint = int(raw[14:16])
    sec = int(raw[17:19])
    ms = int(raw[20:23]) * 1000
    return datetime.datetime(year, mon, day, hour, mint, sec, ms)


@set_func_attr(filetype='ZCtrl')
def loglineformat_default(raw):
    mon = int(raw[0:2])
    day = int(raw[3:5])
    year = int(raw[6:10])
    hour = int(raw[12:14])
    mint = int(raw[15:17])
    sec = int(raw[18:20])
    ms = int(raw[21:24]) * 1000
    return datetime.datetime(year, mon, day, hour, mint, sec, ms)


@set_func_attr(filetype='default')
def loglineformat_default(raw):
    mon = int(raw[0:2])
    day = int(raw[3:5])
    year = int(raw[6:10])
    hour = int(raw[11:13])
    mint = int(raw[14:16])
    sec = int(raw[17:19])
    ms = int(raw[20:26])
    return datetime.datetime(year, mon, day, hour, mint, sec, ms)


