import os       # Operating system
import utils.simple



class Environment:
    homepath = os.getcwd()
    filepath_registry = {'default':      homepath,
                         'test':         os.path.join(homepath, 'test'),
                         'old_server':   '//172.21.162.76/Data_Sync',
                         'new_server':   'fill'}

    input_registry = {'default':      homepath,
                         'test':         os.path.join(homepath, 'sql'),
                         'old_server':   '//172.21.162.76/Data_Sync',
                         'new_server':   'fill'}
    config_registry = {'default':      os.path.join(homepath,'configs'),
                         'test':         os.path.join(homepath, 'sql'),
                         'old_server':   '//172.21.162.76/Data_Sync',
                         'new_server':   'fill'}
    transfer_registry = {'default':      os.path.join(homepath,'transfer'),
                         'test':         os.path.join(homepath, 'sql'),
                         'old_server':   '//172.21.162.76/Data_Sync',
                         'new_server':   'fill'}

    def __init__(self, keyword='default'):
        try:
            self.logfile_path = self.filepath_registry[keyword]
            self.input_registry = self.input_registry[keyword]
        except OSError:
            print("if attempting to access server, log in through file browsing system first")
        except KeyError:
            print("environment not found in library. Key input:" + str(keyword) + " | key library: "
                  + str(self.filepath_registry))
        self.active_tools = os.listdir(self.input_registry)
        self.config_path = self.config_registry[keyword]
        self.web_transfer = self.transfer_registry[keyword]


active_environment = Environment()


def get_tooltype_specs():
    my_path = os.path.join(active_environment.config_path, 'spec.csv')
    return utils.simple.csv_to_nested_dict(my_path)


def get_tooltype():
    my_path = os.path.join(active_environment.config_path, 'toolinfo.csv')
    return utils.simple.csv_to_nested_dict(my_path)


# should read through the tools that have been copied to the transfer directory and use that
class Tool:
    spec_registry = get_tooltype_specs()
    tooltype_registry = get_tooltype()
    # there should be a tool folder, tool

    def __init__(self, toolnick):
        self.toolname = self.tooltype_registry[toolnick]['name']
        self.tooltype = self.tooltype_registry[toolnick]['type']
        self.specs = self.spec_registry[self.tooltype]

    def get_tooltype_specs(self, tooltype):
        return self.spec_registry[''][tooltype]

    @classmethod
    def get_specs_from_name(cls, toolname):
        tooltype = cls.tooltype_registry[toolname]
        spec = cls.spec_registry[tooltype]
        return spec



