# top section is abstract, bottom half is concrete implementation of the two primary databases
# used in this project

import datetime
from utils.simple import none_given

class SqlLogRequest:
    def __init__(self, tool, magic_date, request=None):
        self.date_range = self.date_handler(magic_date)
        self.tool = tool        # can be an array
        self.request = request

    def __call__(self):
        if not isinstance(self.tool, list):
            self.tool = [self.tool]
        if not isinstance(self.request, list):
            self.request = [self.request]
        for this_tool in self.tool:
            for this_request in self.request:
                self.sql_hook(this_tool, this_request)

    def sql_hook(self, tool, request):
        data = str(tool) + str(request) + str(self.date_range)
        # if date range is int grab the most recent x number of that info
        # if request is a filename, grab
        return data

    @staticmethod
    def date_handler(self):
        start, end = datetime.datetime(1, 1, 1, 1, 1, 1), datetime.datetime(1, 1, 1, 1, 1, 1)
        return [start, end]


class SqlRequest:
    pass


sqlrequester = SqlLogRequest()
sqlrequester()


def push_raw():
    return False


def get_raw():

    return False

#todo for sichong
def push_sparse():

    return False


def get_sparse(tool, attr, datestamp=None):
    if none_given(datestamp):
        datestamp = datetime.datetime.now()

    return Attribute()
