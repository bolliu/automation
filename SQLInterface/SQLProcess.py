import time
import os
from utils.simple import reverse_readline
from LogHandling import LogLine, LogFile
from Environment import active_environment
from utils.simple import initialize_register
import AnalyzerCollection
import TriggerCollection
import SQLInterface


def output_delta_logs():
    analyzer = AttributeManager()
    for tool in input_file_directory():
        for filename in tool:
            mypath = os.path.join(tool, filename)
            this_log_file = LogFile(file_raw=filename, path=mypath)
            this_log_file.tool = tool                   #todo make this better
            this_log_file.parse_filename()
            if this_log_file.filename_full not in get_raw_database_directory():
                this_log_file.parse_loglines()
                chuck_logfile = this_log_file
            else:
                this_timestamp = get_from_raw_last_valid_timestamp(this_log_file)
                chuck_logfile = this_log_file.parse_loglines_reverse(mypath, this_timestamp)
            chuck_logfile.tool = tool
            serialize_unique_to_database(chuck_logfile)
            analyzer.process_new_chunk(chuck_logfile)


def serialize_unique_to_database(logfile):
    for line in logfile.lines:
        SQLInterface.push_raw(logfile.tool,  logfile.fullname_full, line)
    pass


def input_file_directory():
    return os.listdir(active_environment.input_registry)


#todo review, consider merging with other
def get_from_raw_last_valid_timestamp(logfile):
        consider_lines = SQLInterface.get_raw(logfile.tool, logfile.filename_full)
        return consider_lines[-1]


def get_raw_database_directory():
    sql_raw_directory = ["what.txt", "something.txt", "huh.txt"]
    return sql_raw_directory


def there_are_updates():
    pass


def end_of_day():
    pass


def daily_process():
    pass

if __name__ == '__main__':
    while True:
        output_delta_logs() if there_are_updates() else None
        daily_process() if end_of_day() else None
    pass

class AttributeManager:
    datahook = []
    analyzer_register = initialize_register(AnalyzerCollection, 'filename')
    trigger_register = initialize_register(TriggerCollection, 'filename')

    def __init__(self, datahook=active_environment.default_datahook):
        self.datahook = datahook
        self.raw = []
        self.attributes = []

    def process_new_chunk(self, chunk):
        # things to do per line
        for line in chunk.lines:
            self.process_line(line)
            self.check_for_triggers(line)
        self.serialize_all()
        pass

    def process_line(self, line):
        self.attributes += self.analyzer_register[line.logtype](line)

    def check_for_triggers(self, line):
        self.attributes.append(self.trigger_register[line.logtype](line))

    def serialize_all(self):
        for atr in self.attributes:
            atr.serialize()
