# Do not add any functions that start with this file's name

from utils.simple import set_func_attr
import datetime


@set_func_attr(name_in_file='PgunItip')
def filenameformat_gun(datestring):
    year = int(datestring[0:4])
    mon = int(datestring[9:11])
    day = 15
    return datetime.date(year, mon, day)


@set_func_attr(name_in_file='EMGdata')
def filenameformat_emg(datestring):
    year = int(datestring[0:4])
    mon = int(datestring[4:6])
    day = int(datestring[6:8])
    return datetime.date(year, mon, day)


@set_func_attr(name_in_file='default')
def filenameformat_default(datestring):
    year = int(datestring[0:4])
    mon = int(datestring[5:7])
    day = int(datestring[8:10])
    return datetime.date(year, mon, day)
