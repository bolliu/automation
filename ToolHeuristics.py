import Environment
import SQLInterface
import json
import datetime

class WebFunnel:
    def __init__(self, toollist=Environment.active_environment.active_tools):
        self.tools = toollist
        self.attribute_names = {'main_alarms',
                                'chamber_pressure',
                                'chamber_turbo_current'
                                'loadlock_pressure',
                                'loadlock_turbo_current',
                                'foreline_pressure',
                                'IDE',
                                'Gun_Itip',
                                'Gun_IP1',
                                'Gun_IP2',
                                'Gun_IP3',
                                'grounding_voltage',
                                'wafer_location',
                                'HV_status',
                                'recipe',
                                'reboot',
                                'software',
                                'firmware',}
        self.datapack = {}
        self.json_out = None
        pass

    def create_datapack(self):
        for this_tool in self.tools:
            print('starting thi for' + str(this_tool))
            tool_datapack = {}
            for this_attribute in self.attribute_names:
                attr = SQLInterface.pull_sparse(tool=this_tool, attr=this_attribute)
                tool_datapack['value'] = attr.value
                tool_datapack['pass'] = attr.nominal
                tool_datapack['range'] = attr.spec
                tool_datapack['time'] = attr.datestamp
            self.datapack[this_tool] = tool_datapack

    @staticmethod
    def json_default(value):
        if isinstance(value, datetime.datetime) or type(value) == type(datetime.date.today()):
            return str(value)
        else:
            try:
                return value.__dict__
            except AttributeError:
                return 'ERROR'

    def json_output(self):  # Build json object and export
        print('Building datetime object')
        temp = {}
        self.datapack = {'sys' + k: v for k, v in temp.items()}
        self.json_out = json.dumps(self.datapack, default=self.json_default, indent=4, sort_keys=True)
        with open(Environment.active_environment.web_transfer, 'w') as json_file:
            json.dump(self.datapack, json_file, default=self.json_default, indent=4, sort_keys=True)

            # my_thi = ToolHeuristics()